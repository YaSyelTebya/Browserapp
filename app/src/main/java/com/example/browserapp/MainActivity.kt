package com.example.browserapp

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.annotation.ColorRes
import androidx.appcompat.app.AppCompatActivity
import com.example.browserapp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private var _binding: ActivityMainBinding? = null
    private val binding
        get() = _binding!!
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityMainBinding.inflate(layoutInflater)

        setContentView(binding.root)

        changeSearchViewColor(DEFAULT_COLOR)

        setSupportActionBar(binding.toolbar)

        binding.openUrlButton.setOnClickListener {
            hideKeyboard(it)
            openUrl()
        }

        setupWebView()
    }

    private fun setupWebView() {
        binding.webView.webViewClient = object: WebViewClient() {
            override fun shouldOverrideUrlLoading(
                view: WebView?,
                request: WebResourceRequest?
            ): Boolean  = false

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                this@MainActivity.updateTitle()
            }
        }
    }

    private fun openUrl() {
        val providedUrl: String = getUrl()
        binding.webView.loadUrl(providedUrl)
    }

    private fun getUrl(): String = binding.input.text.toString()

    fun updateTitle() {
        supportActionBar!!.title = with(binding.webView.title) {
            if(isNullOrEmpty()) {
                getString(R.string.app_name)
            } else {
                this
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when(item.itemId) {
        R.id.action_color1 -> {
            changeSearchViewColor(R.color.color1)
            true
        }
        R.id.action_color2 -> {
            changeSearchViewColor(R.color.color2)
            true
        }
        R.id.action_color3 -> {
            changeSearchViewColor(R.color.color3)
            true
        }
        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    private fun changeSearchViewColor(@ColorRes newColor: Int) {
        binding.openUrlButton.setBackgroundColor(getColor(newColor))
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    private fun hideKeyboard(view: View) {
        val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    companion object {
        private const val TAG = "ColorableBrowser"
        private val DEFAULT_COLOR = R.color.default_color
    }
}